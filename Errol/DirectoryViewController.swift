//
//  ViewController.swift
//  Project 1
//
//  Created by Cody Hill on 1/3/19.
//  Copyright © 2019 Cody Hill. All rights reserved.
//

import UIKit
import CSV

class DirectoryViewController: UITableViewController {
    
    var attendees = [Attendee]()
    var filteredAttendees = [Attendee]()
    
    let searchController = UISearchController(searchResultsController: nil)

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "Attendee Directory"
        
        if let csvURL = Bundle.main.url(forResource: "wave4all", withExtension: "csv") {
            openCSVFile(csvURL)
        }
        
        // Set up Search Bar
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search Attendees"
        
        // TextField Color Customization
        UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).defaultTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        
        navigationItem.searchController = searchController
        definesPresentationContext = true
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isFiltering() {
            return filteredAttendees.count
        }
        
        return attendees.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! AttendeeTableViewCell
        let attendee: Attendee
        
        if isFiltering() {
            attendee = filteredAttendees[indexPath.row]
        } else {
            attendee = attendees[indexPath.row]
        }
        
        cell.nameLabel?.text = String(attendee.firstName + " " + attendee.lastName)
        
        if let phoneNumber = format(phoneNumber: attendee.phoneNumber) {
           cell.phoneNumberLabel.text  = phoneNumber
        } else {
            cell.phoneNumberLabel.text = ""
        }
        
        cell.locationLabel.text = attendee.city + ", " + attendee.state
        
        return cell
    }
    
    override func  tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let vc = storyboard?.instantiateViewController(withIdentifier: "Detail") as? DetailViewController {
            if isFiltering() {
                vc.selectedAttendee = filteredAttendees[indexPath.row]
            } else {
                vc.selectedAttendee = attendees[indexPath.row]
            }

            navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    // For Search Bar implementation
    func isFiltering() -> Bool {
        return searchController.isActive && !searchBarIsEmpty()
    }
    
    func searchBarIsEmpty() -> Bool {
        // return true if the test is empty or nil
        return searchController.searchBar.text?.isEmpty ?? true
    }
    
    func filterContentForSearchTest(_ searchText: String, scope: String = "All") {
        filteredAttendees = attendees.filter({( attendee: Attendee) -> Bool in
            print(attendee.fullName)
            return attendee.fullName.lowercased().contains(searchText.lowercased())
        })
        
        tableView.reloadData()
    }
    
    func openCSVFile(_ url: URL) {
        
        let filename = url.lastPathComponent
        do {
            try processCSVFile(at: url)
            print("Successfully loaded: \(filename)")
        } catch {
            print("Error: Problem reading file: \(filename)")
        }
    }
    
    func processCSVFile(at url: URL) throws {
        let filestring = try String(contentsOf: url)
        // Remove stupid ass crlf bullshit from Windows.
        let carriageReturnStrippedString = filestring.replacingOccurrences(of: "\r", with: "")
        try getAttendees(in: carriageReturnStrippedString, from: url)
    }
    
    func getAttendees(in string: String, from url: URL) throws {
        
        let stream = InputStream(url: url)
        let csv = try! CSVReader(stream: stream!)
        while let row = csv.next() {
            print(row)
            var temp = Attendee()
            temp.UUID = String(row[1])
            temp.firstName = String(row[4])
            temp.lastName = String(row[5])
            temp.fullName = String("\(temp.firstName) \(temp.lastName)")
            temp.phoneNumber = String(row[10])
            temp.city = String(row[11])
            temp.state = String(row[12])
            
            attendees.append(temp)
        }
        
    }

}

extension DirectoryViewController: UISearchResultsUpdating {
    // MARK: - UISearchResultsUpdating Delegate
    func updateSearchResults(for searchController: UISearchController) {
        filterContentForSearchTest((searchController.searchBar.text!))
    }
}
