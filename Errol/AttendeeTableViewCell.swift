//
//  AttendeeTableViewCell.swift
//  Errol
//
//  Created by Cody Hill on 2/22/19.
//  Copyright © 2019 Cody Hill. All rights reserved.
//

import UIKit

class AttendeeTableViewCell: UITableViewCell {
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var phoneNumberLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
