//
//  LegendsAttendee.swift
//  Cerberus
//
//  Created by Cody Hill on 10/9/18.
//  Copyright © 2018 Cody Hill. All rights reserved.
//

import UIKit
import AVFoundation

struct Attendee {
    var UUID: String = ""
    var wave: String = ""
    var show: String = ""
    var firstName: String = ""
    var lastName: String = ""
    var fullName: String = ""
    var city: String = ""
    var state: String = ""
    var phoneNumber: String = ""
    
    init() {
        
    }
    
    init(firstName: String, lastName: String) {
        self.fullName = String("\(firstName) \(lastName)")
    }
    
    
    func qrcode() -> UIImage? {
        if UUID != "" {
            return generateQRCode(from: self.UUID)
        } else {
            return nil
        }
    }
    
    private func generateQRCode(from string: String) -> UIImage? {
        let data = string.data(using: String.Encoding.ascii)
        
        if let filter = CIFilter(name: "CIQRCodeGenerator") {
            filter.setValue(data, forKey: "inputMessage")
            let transform = CGAffineTransform(scaleX: 10, y: 10)
            
            if let output = filter.outputImage?.transformed(by: transform) {
                return UIImage(ciImage: output)
            }
        }
        
        return nil
    }
}
