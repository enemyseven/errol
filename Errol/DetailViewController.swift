//
//  DetailViewController.swift
//  Project 1
//
//  Created by Cody Hill on 1/7/19.
//  Copyright © 2019 Cody Hill. All rights reserved.
//

import UIKit
import MobileCoreServices
import MessageUI

class DetailViewController: UIViewController, MFMessageComposeViewControllerDelegate {
    
    @IBOutlet var imageView: UIImageView!
    
    var selectedAttendee: Attendee?
    
    @IBAction func sendButtonTapped(_ sender: Any) {
        
        //Bad choice. Look Later.
        guard let recipientNumber = selectedAttendee?.phoneNumber else { return }
        
        if (MFMessageComposeViewController.canSendText()) {
            let controller = MFMessageComposeViewController()
            controller.body = "Here is your personal QR Code for Legends of Possible!"
            controller.addAttachmentData((imageView.image?.pngData())!, typeIdentifier: "public.png", filename: "QRCode.png")
            
            //            reference phone number in list if available.
            if [recipientNumber].count == 0 {
                controller.recipients = [""]
            }
            
//            Check if there is a phoneNumber and add it if one is available.
            if recipientNumber != "" {
                controller.recipients = [recipientNumber]
            }
            
            controller.messageComposeDelegate = self
            self.present(controller, animated: true, completion: nil)
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        guard let currentAttendee = selectedAttendee else { return }
        
        imageView.image = drawBadge(for: currentAttendee)

        // Do any additional setup after loading the view.
    }
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        //... handle sms Screen actions
        self.dismiss(animated: true, completion: nil)
    }
    
    func drawBadge(for attendee: Attendee) -> UIImage {
        
        let renderer = UIGraphicsImageRenderer(size: CGSize(width: 512, height: 640))
        
        let img = renderer.image { ctx in
            
            UIColor.white.setFill()
            ctx.fill(CGRect(x: 0, y: 0, width: 512, height: 640))
            
            let paragraphStyle = NSMutableParagraphStyle()
            paragraphStyle.alignment = .center
            
            let attrs = [NSAttributedString.Key.font: UIFont(name: "HelveticaNeue", size: 36)!, NSAttributedString.Key.paragraphStyle: paragraphStyle]
            
            let fullname = String(attendee.firstName + " " + attendee.lastName)
            fullname.draw(with: CGRect(x: 32, y: 32, width: 448, height: 448), options: .usesLineFragmentOrigin, attributes: attrs, context: nil)
            
            attendee.qrcode()!.draw(in: CGRect(x: 128, y: 128, width: 256, height: 256))
            
            let UUID = String(attendee.UUID)
            UUID.draw(with: CGRect(x: 32, y: 448, width: 448, height: 448), options: .usesLineFragmentOrigin, attributes: attrs, context: nil)
            
            
        }
        
        return img
    }
    
//    ---- The following are no longer needed ----
    
    func enlargeCanvas(_ image: UIImage) -> UIImage? {
        
        let expandedSize = CGSize(width: image.size.width + 80, height: image.size.height + 180)
        
        let imageOnBlueCanvas = drawImageOnCanvas(image, canvasSize: expandedSize, canvasColor: .gray)
        
        return imageOnBlueCanvas
    }
    
    func drawImageOnCanvas(_ useImage: UIImage, canvasSize: CGSize, canvasColor: UIColor ) -> UIImage {
        
        let rect = CGRect(origin: .zero, size: canvasSize)
        UIGraphicsBeginImageContextWithOptions(rect.size, false, 0.0)
        
        // fill the entire image
        canvasColor.setFill()
        UIRectFill(rect)
        
        // calculate a Rect the size of the image to draw, centered in the canvas rect
        let centeredImageRect = CGRect(x: (canvasSize.width - useImage.size.width) / 2,
                                       y: ((canvasSize.height - useImage.size.height) / 2) + 16,
                                       width: useImage.size.width,
                                       height: useImage.size.height)
        
        // get a drawing context
        let context = UIGraphicsGetCurrentContext();
        
        // "cut" a transparent rectanlge in the middle of the "canvas" image
        context?.clear(centeredImageRect)
        
        // draw the image into that rect
        useImage.draw(in: centeredImageRect)
        
        // get the new "image in the center of a canvas image"
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return image!
        
    }
    
    func addNameToImage(drawText text: NSString, inImage image: UIImage) -> UIImage {
        UIGraphicsBeginImageContext(image.size)
        image.draw(in: CGRect(x: 0, y: 0, width: image.size.width, height: image.size.height))
        let font = UIFont(name: "HelveticaNeue", size: 24)!
        let text_style = NSMutableParagraphStyle()
        text_style.alignment=NSTextAlignment.center
        let text_color = UIColor.black
        let attributes = [NSAttributedString.Key.font:font, NSAttributedString.Key.paragraphStyle:text_style, NSAttributedString.Key.foregroundColor:text_color]
        let text_h = font.lineHeight
        let text_y = (image.size.height-text_h)/2 - 104
        let text_rect = CGRect(x: 0, y: text_y, width: image.size.width, height: text_h)
        text.draw(in: text_rect.integral, withAttributes: attributes)
        let result=UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return result!
    }
    
    func addUUIDToImage(drawText text: NSString, inImage image: UIImage) -> UIImage {
        UIGraphicsBeginImageContext(image.size)
        image.draw(in: CGRect(x: 0, y: 0, width: image.size.width, height: image.size.height))
        let font = UIFont(name: "HelveticaNeue", size: 16)!
        let text_style = NSMutableParagraphStyle()
        text_style.alignment = NSTextAlignment.center
        let text_color = UIColor.black
        let attributes = [NSAttributedString.Key.font:font, NSAttributedString.Key.paragraphStyle:text_style, NSAttributedString.Key.foregroundColor:text_color]
        let text_h = font.lineHeight
        let text_y = (image.size.height-text_h)/2 + 114
        let text_rect = CGRect(x: 0, y: text_y, width: image.size.width, height: text_h)
        text.draw(in: text_rect.integral, withAttributes: attributes)
        let result=UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return result!
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
